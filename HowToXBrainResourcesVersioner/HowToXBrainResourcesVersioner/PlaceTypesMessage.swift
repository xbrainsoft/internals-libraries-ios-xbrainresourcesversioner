//
//  PlaceTypesMessage.swift
//  HowToXBrainResourcesVersioner
//
//  Created by Christophe Cadix on 15/03/2016.
//  Copyright © 2016 xbrain. All rights reserved.
//

import Foundation

/**
 Custom message received  
 It provides us with up to date resources, along with their identifier and version number.
 */
class PlaceTypesMessage : XBCCustomMessagePrototype {
    
    private static let MessageKey = "Xbrain.Messages.Navigation.PointOfInterestCategoriesResponse"
    private static let MemberKeyResourceId = "ResourceId"
    private static let MemberKeyVersion = "Version"
    private static let MemberKeyCategories = "Categories"
    
    var resourceId: String
    var version: String
    private var categorieMessages: [CategoryMessage]
    var categories: [String : String] {
        var result: [String : String] = [:]
        categorieMessages.forEach { result[$0.id] = $0.name }
        return result
    }
    
    /// Provide the message String identifier
    override class func key() -> String {
        return MessageKey
    }
    
    /// Deserialization (when message is received)
    required init!(coder aDecoder: NSCoder) {
        resourceId = aDecoder.decodeObjectForKey(PlaceTypesMessage.MemberKeyResourceId) as! String
        version = aDecoder.decodeObjectForKey(PlaceTypesMessage.MemberKeyVersion) as! String
        categorieMessages = aDecoder.decodeArrayOfClass(
            CategoryMessage.self,
            forKey: PlaceTypesMessage.MemberKeyCategories) as! [CategoryMessage]
        super.init()
    }
    
    /// Serialization (when message is about to be sent)
    override func encodeWithCoder(aCoder: NSCoder) {
        // this message is never sent, only received
    }
    
    private class CategoryMessage : XBCCustomMessagePrototype {
        private static let MessageKey = "Xbrain.Messages.Navigation.PointOfInterestCategory"
        private static let MemberKeyId = "Id"
        private static let MemberKeyName = "Name"
        
        var id: String
        var name: String
        
        /// Provide the message String identifier
        override class func key() -> String {
            return MessageKey
        }
        
        /// Deserialization (when message is received)
        required init!(coder aDecoder: NSCoder) {
            id = aDecoder.decodeObjectForKey(CategoryMessage.MemberKeyId) as! String
            name = aDecoder.decodeObjectForKey(CategoryMessage.MemberKeyName) as! String
            super.init()
        }
        
        /// Serialization (when message is about to be sent)
        override func encodeWithCoder(aCoder: NSCoder) {
            // this message is never sent, only received
        }
    }
}
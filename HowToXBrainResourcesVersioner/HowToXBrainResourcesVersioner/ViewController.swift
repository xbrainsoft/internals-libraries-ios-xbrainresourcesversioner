//
//  ViewController.swift
//  HowToXBrainResourcesVersioner
//
//  Created by Christophe Cadix on 14/03/2016.
//  Copyright © 2016 xbrain. All rights reserved.
//

import UIKit

/**
 This defines the only one screen of this app. It connects to an agent that handles 3 kinds of versioned resources:
 - some *embedded sentences* (for localization defined server side)
 - some *frequently used phrases* (to provide help to the user)
 - some *POI categories*

 We list them in a table view with their version numbers. If the app has nothing stored yet the default version number will
 be `0.0.0`.
 
 #### Register
 At application startup resources are registered to the `XBrainResourcesVersioner` library as follows:  
 `XBRVManager.add(versioned)`

 #### Loading
 Our resources may have already been stored on local storage. We need to load them before connecting to the agent. It is done
 (right after having them registered) as follows:  
 `XBRVManager.loadResources()`
 
 #### Agent requests version numbers
 Once the connection to the agent (triggered by tapping the *Connect* button) is established it will request us to provide
 the version numbers of those resources. That will be **entirely handled** by the `XBrainResourcesVersioner` library.  

 #### When our resources are outdated
 If our resources are outdated (or simply never received) the agent will send to us (through some specific custom messages)
 their up to date version. We must then submit those information (identifier and version number) to the
 `XBrainResourcesVersioner` library. Of course we need to know those specific custom messages and register to them. All of
 this is done in `EmbeddedSentences.swift`, `FrequentlyUsedPhrases.swift` and `PlaceTypes.swift`.
*/
class ViewController: UIViewController, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var versionLabel: UILabel!
    
    private let resources: [XBRVVersionedResource] = [
        EmbeddedSentences(),
        FrequentlyUsedPhrases(),
        PlaceTypes()
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NSLog("configuration = \(AppConfig.configuration)")
        
        let versionNumber = XBRVManager.versionNumber()
        let buildNumber = XBRVManager.buildNumber()
        versionLabel.text = "XBrainResourcesVersioner-\(versionNumber) (\(buildNumber))"
        
        observeResourceChange()
        
        tableView.dataSource = self
        
        // register our resources and load them
        for versioned in resources {
            XBRVManager.add(versioned)
        }
        XBRVManager.loadResources()
        
        observeConnectionStatusChange()
        initXBrainClient()
    }
    
    /// Action attached to the *Connect/Disconnect* button
    @IBAction private func connect(sender: AnyObject) {
        if !XBCClient.isConnected() {
            NSLog("About to connect...")
            XBCClient.connect()
        } else {
            NSLog("Already connected")
        }
    }
    
    func observeResourceChange() {
        // fire a notification so that the UI updates itself
        NSNotificationCenter.defaultCenter().addObserver(
            self, selector: #selector(onResourceChanged(_:)), name: AppDelegate.ResourceWasUpdated, object: nil)
    }
    
    @objc private func onResourceChanged(notification: NSNotification?) {
        NSLog("onResourceChanged...")
        tableView.reloadData()
    }
    
    //MARK: Agent
    
    /// *XBrainClient* configuration
    private func initXBrainClient() {
        if let agentConfig = AppConfig.agentConfig {
            XBCClient.configure(agentConfig)
        } else {
            NSLog("Was unable to configure XBCClient: no agent config found")
        }
    }

    
    /// Computed property giving the connection state
    private var connectionState: kXBCConnectionState {
        return XBCClient.connectionState()
    }
    
    /// When the connection state changes we log it. Media synchronization is started when connected.
    func onConnectionStateChanged(notification: NSNotification?) {
        switch connectionState {
            case .XBCConnecting: NSLog("[connecting to agent...]")
            case .XBCDisconnected: NSLog("[disconnected from agent]")
            case .XBCConnected: NSLog("[connected to agent]")
            case .XBCConnectedAnonymously: NSLog("[anonymously connected to agent]")
        }
    }
    
    private func observeConnectionStatusChange(enabled: Bool = true) {
        let name = kXBCConnectionStateChanged
        if (enabled) {
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(onConnectionStateChanged(_:)),
                                                             name: name, object: nil);
        } else {
            NSNotificationCenter.defaultCenter().removeObserver(self, name: name, object: nil)
        }
    }
    
    //MARK: UITableViewDataSource
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return resources.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier("ResourceDetail", forIndexPath:indexPath)
        let i = indexPath.row
        let resourceName: String
        let resourceVersion: String
        if i < resources.count {
            let resource: XBRVVersionedResource = resources[i]
            resourceName = resource.userName()
            resourceVersion = XBRVManager.getVersionOf(resource)
        } else {
            resourceName = "Unknown"
            resourceVersion = "Unknown"
        }
        cell.textLabel?.text = resourceName
        cell.detailTextLabel?.text = resourceVersion
        return cell;
    }
}

//
//  EmbeddedSentences.swift
//  HowToXBrainResourcesVersioner
//
//  Created by Christophe Cadix on 14/03/2016.
//  Copyright © 2016 xbrain. All rights reserved.
//

import Foundation

/**
 Embedded Sentences  
 It represents a versioned resource in our app by conforming to the `XBRVVersionedResource` protocol, made easy by using a
 `XBRVHelper`.  
 It registers to `EmbeddedSentencesMessage` reception so that it can:
 - update its model
 - submit the received identifier and version number to the `XBrainResourcesVersioner` library.
*/
class EmbeddedSentences : NSObject, XBRVVersionedResource {
    
    typealias DataType = [String : String]
    
    private let StoreKeyName = "resource_embedded_sentences"
    private let UserName = "Embedded Sentences"
    
    private var sentences: DataType = [:]
    private var helper: XBRVHelper!
    
    override init() {
        super.init()
        helper = XBRVHelper(key: StoreKeyName, resource: self)
        XBCClient.registerMessageHandler(#selector(onMessageReceived(_:)), target: self, forMessageClass: EmbeddedSentencesMessage.self)
    }
    
    /**
     Provide a user friendly name for this resource
     */
    func userName() -> String {
        return UserName;
    }
    
    /**
     Load resource from local storage
     - returns: whether it succeeded or not
     */
    func loadResource() -> Bool {
        if let dictionary = helper.load() as? DataType {
            sentences = dictionary
            return true
        }
        return false
    }
    
    /**
     Save on local storage our model data
     - important: your model data should not been altered when this method fails and returns false.
     - parameter identifier: the resource's identifier
     - parameter version: the resource's version number
     - returns: whether it succeeded to save on the local storage or not
     */
    func saveResource(identifier: String, version: String) -> Bool {
        return helper.save(identifier, version: version, values: sentences)
    }
    
    //MARK: Message reception
    @objc private func onMessageReceived(message: EmbeddedSentencesMessage) {
        NSLog("Received EmbeddedSentencesMessage")
        
        // Update our model
        sentences = message.sentences
        
        // Submit versioning information to `XBrainResourcesVersioner` library
        saveResource(message.resourceId, version: message.version)
        
        // fire a notification so that the UI updates itself
        NSNotificationCenter.defaultCenter().postNotificationName(AppDelegate.ResourceWasUpdated, object: nil)
    }
}
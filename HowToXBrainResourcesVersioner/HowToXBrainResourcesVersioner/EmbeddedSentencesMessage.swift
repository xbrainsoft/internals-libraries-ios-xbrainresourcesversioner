//
//  EmbeddedSentencesMessage.swift
//  HowToXBrainResourcesVersioner
//
//  Created by Christophe Cadix on 15/03/2016.
//  Copyright © 2016 xBrainSoft. All rights reserved.
//

/**
 Custom message received  
 It provides us with up to date resources, along with their identifier and version number.
*/
class EmbeddedSentencesMessage: XBCCustomMessagePrototype {
    
    private static let MessageKey = "Xbrain.Messages.Data.EmbeddedSentences"
    private static let MemberKeyResourceId = "ResourceId"
    private static let MemberKeyVersion = "Version"
    private static let MemberKeySentences = "Sentences"
    
    var resourceId: String
    var version: String
    private var sentenceMessages: [SentenceMessage]
    var sentences: [String : String] {
        var result: [String : String] = [:]
        sentenceMessages.forEach { result[$0.id] = $0.sentence }
        return result
    }
    
    /// Provide the message String identifier
    override class func key() -> String {
        return MessageKey
    }
    
    /// Deserialization (when message is received)
    required init!(coder aDecoder: NSCoder) {
        resourceId = aDecoder.decodeObjectForKey(EmbeddedSentencesMessage.MemberKeyResourceId) as! String
        version = aDecoder.decodeObjectForKey(EmbeddedSentencesMessage.MemberKeyVersion) as! String
        sentenceMessages = aDecoder.decodeArrayOfClass(
            SentenceMessage.self,
            forKey: EmbeddedSentencesMessage.MemberKeySentences) as! [SentenceMessage]
        super.init()
    }
    
    /// Serialization (when message is about to be sent)
    override func encodeWithCoder(aCoder: NSCoder) {
        // this message is never sent, only received
    }
    
    private class SentenceMessage : XBCCustomMessagePrototype {
        private static let MessageKey = "Xbrain.Messages.Data.EmbeddedSentence"
        private static let MemberKeyId = "Id"
        private static let MemberKeySentence = "Sentence"
        
        var id: String
        var sentence: String
        
        /// Provide the message String identifier
        override class func key() -> String {
            return MessageKey
        }
        
        /// Deserialization (when message is received)
        required init!(coder aDecoder: NSCoder) {
            id = aDecoder.decodeObjectForKey(SentenceMessage.MemberKeyId) as! String
            sentence = aDecoder.decodeObjectForKey(SentenceMessage.MemberKeySentence) as! String
            super.init()
        }
        
        /// Serialization (when message is about to be sent)
        override func encodeWithCoder(aCoder: NSCoder) {
            // this message is never sent, only received
        }
    }
}
//
//  PlaceTypes.swift
//  HowToXBrainResourcesVersioner
//
//  Created by Christophe Cadix on 14/03/2016.
//  Copyright © 2016 xbrain. All rights reserved.
//

import Foundation

/**
 POI Categories  
 It represents a versioned resource in our app by conforming to the `XBRVVersionedResource` protocol, made easy by using a
 `XBRVHelper`.  
 It registers to `PlaceTypesMessage` reception so that it can:
 - update its model
 - submit the received identifier and version number to the `XBrainResourcesVersioner` library.
*/
class PlaceTypes : NSObject, XBRVVersionedResource {
    
    typealias DataType = [String : String]
    
    private let StoreKeyName = "resource_poi_categories"
    private let UserName = "POI Categories"
    
    private var categories: DataType = [:] // categories of POI
    private var helper: XBRVHelper!
    
    override init() {
        super.init()
        helper = XBRVHelper(key: StoreKeyName, resource: self)
        XBCClient.registerMessageHandler(#selector(onMessageReceived(_:)), target: self, forMessageClass: PlaceTypesMessage.self)
    }
    
    /**
     Provide a user friendly name for this resource
     */
    func userName() -> String {
        return UserName
    }
    
    /**
     Load resource from local storage
     - returns: whether it succeeded or not
     */
    func loadResource() -> Bool {
        if let dictionary = helper.load() as? DataType {
            categories = dictionary
            return true
        }
        return false
    }
    
    /**
     Save on local storage our model data
     - important: your model data should not been altered when this method fails and returns false.
     - parameter identifier: the resource's identifier
     - parameter version: the resource's version number
     - returns: whether it succeeded to save on the local storage or not
     */
    func saveResource(identifier: String, version: String) -> Bool {
        return helper.save(identifier, version: version, values: categories)
    }
    
    //MARK: Message reception
    @objc private func onMessageReceived(message: PlaceTypesMessage) {
        NSLog("Received PlaceTypesMessage")
        
        // Update our model
        categories = message.categories
        
        // Submit versioning information to `XBrainResourcesVersioner` library
        saveResource(message.resourceId, version: message.version)
        
        // fire a notification so that the UI updates itself
        NSNotificationCenter.defaultCenter().postNotificationName(AppDelegate.ResourceWasUpdated, object: nil)
    }
}
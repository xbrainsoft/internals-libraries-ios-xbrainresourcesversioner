//
//  FrequentlyUsedPhrasesMessage.swift
//  HowToXBrainResourcesVersioner
//
//  Created by Christophe Cadix on 15/03/2016.
//  Copyright © 2016 xbrain. All rights reserved.
//

import Foundation

/**
 Custom message received  
 It provides us with up to date resources, along with their identifier and version number.
 */
class FrequentlyUsedPhrasesMessage : XBCCustomMessagePrototype {
    
    private static let MessageKey = "Xbrain.Messages.Data.FrequentlyUsedPhraseCollection"
    private static let MemberKeyResourceId = "ResourceId"
    private static let MemberKeyVersion = "Version"
    private static let MemberKeyPhrases = "FrequentlyUsedPhrases"
    
    var resourceId: String
    var version: String
    private var phrasesByContextMessages: [PhrasesByContextMessage]
    var phrasesByContext: [String : [String]] {
        var result: [String : [String]] = [:]
        phrasesByContextMessages.forEach { result[$0.context] = $0.phrases }
        return result
    }
    
    /// Provide the message String identifier
    override class func key() -> String {
        return MessageKey
    }
    
    /// Deserialization (when message is received)
    required init!(coder aDecoder: NSCoder) {
        resourceId = aDecoder.decodeObjectForKey(FrequentlyUsedPhrasesMessage.MemberKeyResourceId) as! String
        version = aDecoder.decodeObjectForKey(FrequentlyUsedPhrasesMessage.MemberKeyVersion) as! String
        phrasesByContextMessages = aDecoder.decodeArrayOfClass(
            PhrasesByContextMessage.self,
            forKey: FrequentlyUsedPhrasesMessage.MemberKeyPhrases) as! [PhrasesByContextMessage]
        super.init()
    }
    
    /// Serialization (when message is about to be sent)
    override func encodeWithCoder(aCoder: NSCoder) {
        // this message is never sent, only received
    }
    
    private class PhrasesByContextMessage : XBCCustomMessagePrototype {
        private static let MessageKey = "Xbrain.Messages.Data.FrequentlyUsedPhraseCollection.PhrasesByContext"
        private static let MemberKeyContext = "Context"
        private static let MemberKeyPhrases = "Phrases"
        
        var context: String
        var phrases: [String]
        
        /// Provide the message String identifier
        override class func key() -> String {
            return MessageKey
        }
        
        /// Deserialization (when message is received)
        required init!(coder aDecoder: NSCoder) {
            context = aDecoder.decodeObjectForKey(PhrasesByContextMessage.MemberKeyContext) as! String
            phrases = aDecoder.decodeObjectForKey(PhrasesByContextMessage.MemberKeyPhrases) as! [String]
            super.init()
        }
        
        /// Serialization (when message is about to be sent)
        override func encodeWithCoder(aCoder: NSCoder) {
            // this message is never sent, only received
        }
    }
}
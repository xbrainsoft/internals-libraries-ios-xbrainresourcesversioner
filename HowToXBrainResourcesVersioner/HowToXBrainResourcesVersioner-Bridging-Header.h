//
//  HowToXBrainResourcesVersioner-Bridging-Header.h
//  HowToXBrainResourcesVersioner
//
//  Created by Christophe Cadix on 14/03/2016.
//  Copyright © 2015 xBrainSoft. All rights reserved.
//

#ifndef HowToXBrainResourcesVersioner_Bridging_Header_h
#define HowToXBrainResourcesVersioner_Bridging_Header_h

#import "XBrainResourcesVersioner.h"
#import "XBrainClient.h"

#endif /* HowToXBrainResourcesVersioner_Bridging_Header_h */

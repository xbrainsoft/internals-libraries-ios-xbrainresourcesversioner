Pod::Spec.new do |s|  
  s.name         = "XBrainResourcesVersioner"
  s.version      = "1.0.2"
  s.summary      = "XBrain framework for resources versioning management"
  s.homepage     = "http://xbrain.io"
  s.license      = ''
  s.author       = { "Christophe Cadix" => "christophe.cadix@xbrain.io" }
  s.platform     = :ios, '8.0'
  s.requires_arc = true

  #### uncomment in development mode (i.e. pod locally referenced)
  #### comment out otherwise
  #s.source_files  = 'XBrainResourcesVersioner', 'XBrainResourcesVersioner/**/*.{h,m}'
  #s.public_header_files = 'XBrainResourcesVersioner/**/*.h'
  ####
  
  #### comment in development mode (i.e. pod locally referenced)
  #### uncomment otherwise
  s.source_files = 'XBrainResourcesVersioner/XBrainResourcesVersioner.framework/Headers/*.h'
  s.vendored_frameworks = 'XBrainResourcesVersioner/XBrainResourcesVersioner.framework'
  ####

  s.source = { :git => "https://bitbucket.org/xbrainsoft/internals-libraries-ios-xbrainresourcesversioner.git", :tag => s.version }

  s.dependency 'XBrainClient', '~> 1.4'

end  

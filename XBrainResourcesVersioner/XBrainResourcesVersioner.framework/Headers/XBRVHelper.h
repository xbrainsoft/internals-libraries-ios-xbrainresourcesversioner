//
//  XBRVHelper.h
//  XBrainResourcesVersioner
//
//  Created by Christophe Cadix on 10/03/2016.
//  Copyright © 2016 xbrain. All rights reserved.
//

#import "XBRVVersionedResource.h"

/**
 * Helper class whose purpose is to take care of the implementation of the <i>XBRVVersionedResource</i> protocol by storing
 * data (<i>Property List</i>-compatible) using <i>NSUserDefaults</i>.
 * @discussion It handles:<ul>
             <li>writing to/reading from the underlying file where the resources are stored</li>
             <li>updating their (in memory) version numbers by submitting them to <i>XBRVManager</i></li>
             </ul>
 * @param ObjectType the type parameter representing those resources' data type. <b>It must be a Property List type</b>
 * @param VersionedType the type of the versioned resources
 */
@interface XBRVHelper<ObjectType, VersionedType : id<XBRVVersionedResource>> : NSObject
NS_ASSUME_NONNULL_BEGIN

- (instancetype)init NS_UNAVAILABLE;

/**
 * Designated initializer
 * @param key the name of the private file used to store the resources.
 * @param resource the resource (will be weakly referenced)
 */
- (instancetype)initWithKey:(NSString*)key
                   resource:(id<XBRVVersionedResource>)resource NS_DESIGNATED_INITIALIZER;

/**
 * Load and return the stored resources.
 * @note You should update your model <b>if and only if</b> this methods succeeds (ie. does not return nil).
 * @return the loaded property if it succeeded to do so, nil otherwise.
 */
- (nullable ObjectType)load;

/**
 * Save the given resources by storing them (and their identifier and version number) into the underlying file.
 * @discussion You should update your model with given data <b>even if this method fails</b> (ie. returns <i>NO</i>).
 * @param identifier the resource's identifier
 * @param version the version number
 * @param values the actual data
 * @return whether it succeeded or not
 */
- (BOOL)save:(NSString*)identifier
     version:(NSString*)version
      values:(ObjectType)values;

NS_ASSUME_NONNULL_END
@end
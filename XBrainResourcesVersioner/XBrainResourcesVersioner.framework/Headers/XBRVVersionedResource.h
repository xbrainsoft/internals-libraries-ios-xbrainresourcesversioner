//
//  XBRVVersionedResource.h
//  XBrainResourcesVersioner
//
//  Created by Christophe Cadix on 10/03/2016.
//  Copyright © 2016 xbrain. All rights reserved.
//
//

#import <Foundation/Foundation.h>

/**
 Protocol defining objects that are versioned, that is, data provided to us by the server with an identifier and a version
 number.
 @discussion In order to avoid that data to be transmitted through the network every time the application is started we
             save/load it to/from the local storage.
             <p>Conforming classes:<ul>
             <li><b>must</b> register themselves through: @code XBRVManager#add:(id<XBRVVersionedResource>) @endcode</li>
             <li>should <b>not</b> call: @code loadResource @endcode It is automatically called for them</li>
             <li><b>must</b> call @code saveResource:(NSString*) version: (NSString*) @endcode asap once they received their
                data</li>
             <li>may conform to this protocol by delegating to a <i>XBRVHelper</i></li>
             </ul>
             <p>It is worth noting that conforming classes do not have to synchronize those methods' execution since they are
             not supposed to be executed at the same time (see <i>XBRVManager</i>).
 */
@protocol XBRVVersionedResource <NSObject>
NS_ASSUME_NONNULL_BEGIN

/**
 * Get the name of the resource. It might be shown to user.
 */
- (NSString *)userName;

/**
 * Load resource from local storage.
 * @note This method should not be called directly.
 * @discussion <b>If and only if</b> the data is successfully loaded from cache:<ul>
             <li>implementation should set the resource identifier and the version of the data through:
                @code XBRVManager.put(String, String) @endcode</li>
             <li>you should update your model</li>
             </ul>
 * @return Whether the loading succeeded or not.
 */
- (BOOL)loadResource;

/**
 Save resource to local storage.
 @note This method should be called as soon as possible after the resource has been received.
 @discussion <b>Regardless of whether the data is successfully saved</b>:<ul>
           <li>implementation should set the resource identifier and the version of the data through:
              @code XBRVManager.put(String, String) @endcode</li>
           <li>you should update your model</li>
           </ul>
 @param identifier resource's identifier
 @param version resource's version number
 @return Whether the saving succeeded or not.
 */
- (BOOL)saveResource:(NSString *)identifier version:(NSString *)version;

NS_ASSUME_NONNULL_END
@end



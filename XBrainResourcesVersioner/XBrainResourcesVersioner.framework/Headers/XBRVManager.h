//
//  XBRVManager.h
//  XBrainResourcesVersioner
//
//  Created by Christophe Cadix on 10/03/2016.
//  Copyright © 2016 xbrain. All rights reserved.
//

#import "XBRVVersionedResource.h"

NS_ASSUME_NONNULL_BEGIN

/**
 * Constant to be used to pupulate a response to the server when it requests us to provide the version number of a given
 * resource and there is none.
 */
FOUNDATION_EXPORT NSString * const NULL_VERSION_NUMBER;

/**
 * Main manager
 * @discussion It has two purposes:<ol>
 * <li>store and retrieve version numbers of the resources we were supplied by the agent</li>
 * <li>load stored resources</li>
 * </ol>
 * <b>Usage</b>
 * <p>Once connected to the agent we will be asked the version of the resources we have in storage, thus we need to load them
 * as soon as possible.</p>
 * <p>Once we have provided the agent with our version numbers, either we are up to date and we're all good or we are not and
 * the agent will send us the updated resources with their identifier and version number.</p>
 * <p>Therefore, <b>at application start up, before connecting to the agent</b>, you should:<ul>
 * <li>declare your <i>VersionedResource</i>s through @code add:(id<XBRVVersionedResource>) @endcode</li>
 * <li><b>then</b> call @code loadResources @endcode to load the stored resources</li>
 * </ul>
 * <p>Identifier and version number are always stored in memory.</p>
 */
@interface XBRVManager : NSObject

/**
 * Returns the current version number
 */
+ (NSString*) versionNumber;

/**
 * Returns the current build number
 */
+ (NSString *)buildNumber;

/**
 * Is the resources loading process currently running ?
 */
+ (BOOL)isLoading;

/**
 * Has the resources loading process already been done ?
 */
+ (BOOL)isLoaded;

/**
 * Declare a "versioned resource".
 * @note Note that it may fail if the resources loading process has already started (currently running or finished).
 * @param versioned a <i>VersionedResource</i>
 * @return Whether it could be added or not.
 */
+ (BOOL)add:(id<XBRVVersionedResource>)versioned;

/**
 * Load resources that have been received in a previous session and stored to the local storage.
 * @note This method should be called at application startup, before being connected to the agent, and should never be
 * called again.
 */
+ (void)loadResources;

/**
 * Store in memory the version number of a loaded resource.
 * @note Note that by <i>loaded</i> we mean in memory. In other words it is the (in memory) model's version number and not
 *       the stored version number (which might be different if we failed to write/read it).
 * @param version the resource's version number
 * @param resourceId the resource's identifier
 * @param resource the versioned resource (will be weakly referenced)
 */
+ (void)putVersion:(NSString*)version identifier:(NSString*)resourceId resource:(id<XBRVVersionedResource>)resource;

/**
 * Get the version number of a loaded resource (given its identifier).
 * @note Note that by <i>loaded</i> we mean in memory. In other words it is the (in memory) model's version number and not
 *       the stored version number (which might be different if we failed to write/read it).
 * @param resourceId the resource's identifier
 */
+ (NSString*)getVersionOfIdentifier:(NSString*)resourceId;

/**
 * Get the version number of a loaded resource.
 * @note Note that by <i>loaded</i> we mean in memory. In other words it is the (in memory) model's version number and not
 *       the stored version number (which might be different if we failed to write/read it).
 * @param versioned the versioned resource
 */
+ (NSString *)getVersionOf:(id<XBRVVersionedResource>)versioned;

NS_ASSUME_NONNULL_END
@end
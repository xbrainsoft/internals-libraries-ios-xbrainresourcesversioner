//
//  XBrainResourcesVersioner.h
//  XBrainResourcesVersioner
//
//  Created by Christophe Cadix on 10/03/2016.
//  Copyright © 2016 xbrain. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for XBrainResourcesVersioner.
FOUNDATION_EXPORT double XBrainResourcesVersionerVersionNumber;

//! Project version string for XBrainResourcesVersioner.
FOUNDATION_EXPORT const unsigned char XBrainResourcesVersionerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <XBrainResourcesVersioner/PublicHeader.h>

#import <XBrainResourcesVersioner/XBRVVersionedResource.h>
#import <XBrainResourcesVersioner/XBRVManager.h>
#import <XBrainResourcesVersioner/XBRVHelper.h>
